function tableCreate() {
    var temp = document.querySelector(".MyTable");
    if(temp != null){
        temp.parentNode.removeChild(temp);
    }
  var body = document.body,
    rows = document.getElementById("Row").value,
    columns = document.getElementById("column").value,
    tbl = document.createElement("table");  
    tbl.classList.add("MyTable");
  tbl.style.margin = "15px 100px";
  tbl.style.borderCollapse = "collapse";
  tbl.onclick = function(event){
    let tdr = event.target.closest('td'); // (1)
  if (!tdr) return; 

  if (!tbl.contains(tdr)) return;

  SwitchColor(tdr); // (4)
};

  for (var i = 0; i < rows; i++) {
    var tr = tbl.insertRow();
    for (var j = 0; j < columns; j++) {
      var td = tr.insertCell();
      td.style.height = "30px";
      td.style.textAlign = "center";
      td.style.border = "1px solid black";
    }
  }
  body.appendChild(tbl);
}

function FillTheTable(){
  var tbl = document.querySelector(".MyTable"),
     rows = tbl.getElementsByTagName("tr");
    for(var i = 0;i<rows.length;i++){
      var columns = rows[i].getElementsByTagName("td");
      for(var j =0;j<columns.length;j++)
        columns[j].innerHTML = `${i+1}${j+1}`;
    }
}

function SwitchColor(target){
  var o = Math.round, r = Math.random, s = 100,
  color = 'rgba(' + o(r()*s) + ',' + o(r()*s) + ',' + o(r()*s) + ',' + r().toFixed(1) + ')';
  if(target.style.backgroundColor === target.parentNode.style.backgroundColor){
    target.style.backgroundColor = color;
  }
  else{
    target.style.backgroundColor = target.parentNode.style.backgroundColor;
  }
}